import { Po } from "../models/post";
import { Comm } from "../models/post";

export async function getcom() {
  const resp = await fetch(
    "https://jsonplaceholder.typicode.com/posts/1/comments"
  );
  const dat = await resp.json();
  return new Comm(dat[0].name, dat[0].email, dat[0].body);
}

export async function getpo() {
  const response = await fetch("https://jsonplaceholder.typicode.com/posts/1");
  const data = await response.json();
  return new Po(data.id, data.title, data.body);
}
