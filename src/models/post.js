export class Po {
  constructor(id, title, body) {
    this.id = id;
    this.title = title;
    this.body = body;
  }
}

export class Comm {
  constructor(name, email, body) {
    this.name = name;
    this.email = email;
    this.body = body;
  }
}
